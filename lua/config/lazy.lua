local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not (vim.uv or vim.loop).fs_stat(lazypath) then
  -- bootstrap lazy.nvim
  -- vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
  -- using fork with force_url support
  -- stylua: ignore
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/markus-oberhumer-forks/folke--lazy.nvim", "--branch=force_url", lazypath })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  spec = {
    -- using forks with force_url
    {
      "folke/lazy.nvim",
      name = "lazy.nvim",
      force_url = "https://github.com/markus-oberhumer-forks/folke--lazy.nvim",
      force_branch = "force_url",
    },
    {
      "nvim-lualine/lualine.nvim",
      name = "lualine.nvim",
      force_url = "https://github.com/markus-oberhumer-forks/nvim-lualine--lualine.nvim",
      force_branch = "mfx",
    },
    {
      "nvim-treesitter/nvim-treesitter",
      name = "nvim-treesitter",
      force_url = "https://github.com/markus-oberhumer-forks/nvim-treesitter--nvim-treesitter",
      force_branch = "mfx",
    },
    -- add LazyVim and import its plugins
    { "LazyVim/LazyVim", import = "lazyvim.plugins" },
    -- import/override with your plugins
    { import = "plugins" },
  },
  defaults = {
    -- By default, only LazyVim plugins will be lazy-loaded. Your custom plugins will load during startup.
    -- If you know what you're doing, you can set this to `true` to have all your custom plugins lazy-loaded by default.
    lazy = false,
    -- It's recommended to leave version=false for now, since a lot the plugin that support versioning,
    -- have outdated releases, which may break your Neovim install.
    version = false, -- always use the latest git commit
    -- version = "*", -- try installing the latest stable version for plugins that support semver
  },
  install = { colorscheme = { "tokyonight", "habamax" } },
  checker = { enabled = true }, -- automatically check for plugin updates
  performance = {
    rtp = {
      -- disable some rtp plugins
      disabled_plugins = {
        "gzip",
        -- "matchit",
        -- "matchparen",
        -- "netrwPlugin",
        "tarPlugin",
        "tohtml",
        "tutor",
        "zipPlugin",
      },
    },
  },
})
